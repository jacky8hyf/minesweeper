/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elsk.minesweeper.test;

import com.elsk.minesweeper.control.Grid;
import com.elsk.minesweeper.control.Space;
import java.util.HashSet;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Jacky
 */
public class GetBlockTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Random rand = new Random();
        int row = 20, col = 20,
                numMine = 40;
        Space firstClick = new Space(rand.nextInt(row), rand.nextInt(col));
        com.elsk.minesweeper.control.Grid g = new com.elsk.minesweeper.control.Grid(row, col, numMine, firstClick);
        System.out.println(g);
        Scanner scan = new Scanner(System.in);
        int r = 0, c = 0;
        while (r != -1 && c != -1) {
            System.out.print("r,c:");
            r = scan.nextInt();
            c = scan.nextInt();
            HashSet<Space> hs = g.getBlockAround(new Space(r, c));
            System.out.println(hs);
            String s = new String();
            for (int i = 0; i < row; i++) {
                for (int j = 0; j < col; j++) {
                    s += (
                            hs.contains(new Space(i,j))? "M" : (
                            hasMine(g, i, j) ? "*" :(
                            g.getElementAt(new Space(i,j)) == g.EMPTY ? "_" :(
                            g.getElementAt(new Space(i,j))
                         )))) + " ";
                }
                s += "\n";
            }
            System.out.println(s);
        }
    }
    
    private static boolean hasMine(Grid g, int r, int c) {
        Space sp = new Space(r, c);
        return isValid(g, sp) && g.getElementAt(sp) == g.MINE;
    }

    private static boolean isValid(Grid g, Space sp) {
        int r = sp.getRow(), c = sp.getCol();
        return r >= 0 && r < g.getRow() && c >= 0 && c < g.getCol();
    }
}
