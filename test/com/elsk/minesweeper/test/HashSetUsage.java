/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elsk.minesweeper.test;

import java.util.HashSet;

/**
 *
 * @author Jacky
 */
public class HashSetUsage {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        HashSet<Integer> hs = new HashSet();
        hs.add(5);
        hs.add(6);
        hs.add(5);
        HashSet<Integer> hs1 = new HashSet(); hs1.add(1); hs1.add(2); hs1.add(6);
        System.out.println(hs);
        System.out.println(hs1);
        hs.addAll(hs1);
        System.out.println(hs);
        System.out.println(hs1);
        for(int s: hs)System.out.println(s+" ");
    }
}
