
package com.elsk.minesweeper.test;

import com.elsk.minesweeper.control.PlayBoard;
import com.elsk.minesweeper.control.GameStatusException;
import com.elsk.minesweeper.control.Space;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Jacky
 */
public class PlayBoardTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        Random rand = new Random();
        int row = 9, col = 9,
                numMine = 10;
        System.out.println("Row="+row+" Col="+col+" numMine="+numMine);
        PlayBoard pb = new com.elsk.minesweeper.control.PlayBoard(row, col, numMine); 
        Scanner scan = new Scanner(System.in);
        boolean stop=false;
        while (!stop) {
            System.out.println(pb);
            System.out.print("command row column:");
            String st[] = scan.nextLine().split(" ");
            String command = st[0];
            int r = Integer.parseInt(st[1]), c = Integer.parseInt(st[2]);
            try {
                if("l".equalsIgnoreCase(command))pb.leftClickOn(new Space(r,c));
                else if ("lr".equalsIgnoreCase(command) || "rl".equalsIgnoreCase(command))
                    pb.leftRightClickOn(new Space(r,c));
                else if ("r".equalsIgnoreCase(command)) pb.rightClickOn(new Space(r,c));
            } catch (GameStatusException ex) {
                System.out.println(ex.getMessage());
                stop=true;
            }
        }
    }
}
