
package com.elsk.minesweeper.test;

import com.elsk.minesweeper.control.Grid;
import com.elsk.minesweeper.control.Space;
import java.util.Random;

/**
 *
 * @author Jacky
 */
public class GenerateGridTest {

    public static void main(String[] args) {
        Random rand = new Random();
        for (int i = 0; i < 500; i++) {
            int row = rand.nextInt(50) + 1, col = rand.nextInt(50) + 1,
                    numMine = rand.nextInt(row * col);
            Space firstClick = new Space(rand.nextInt(row), rand.nextInt(col));
            com.elsk.minesweeper.control.Grid g = new com.elsk.minesweeper.control.Grid(row, col, numMine, firstClick);
            System.out.print(i+" "+testGrid(g, row, col, numMine, firstClick)+(i%20==0?"OK\n"+g.toString()+ "-------------\n":""));
        }
    }

    private static String testGrid(Grid g, int row, int col, int numMine, Space firstClick) {
        if (g.getCol() != col || g.getRow() != row)
            return "Col & Row Number Incorrect\n" + g.toString() + "-------------\n";
        if (g.getElementAt(firstClick) == g.MINE) 
            return "Mine in first click space\n" + g.toString() + "-------------\n";
        int tempNumMine = 0;
        for (int i = 0; i < row; i++) for (int j = 0; j < col; j++) 
                if (g.getElementAt(new Space(i, j)) == g.MINE) 
                    tempNumMine++;
        if (tempNumMine != numMine)
            return "Mine Number Incorrect\n" + g.toString() + "-------------\n";
        for (int i = 0; i < row; i++) for (int j = 0; j < col; j++) 
            if(!hasMine(g, i, j) && sumSurroundingMines(g, new Space(i,j))!=g.getElementAt(new Space(i,j)))
                return "At "+i+", "+j+" number is incorrect\n" + g.toString() + "-------------\n";
        return new String();
    }

    private static byte sumSurroundingMines(Grid g, Space sp) {
        byte sum = 0;
        int r = sp.getRow(), c = sp.getCol();
        for (int i = r - 1; i <= r + 1; i++) 
            for (int j = c - 1; j <= c + 1; j++)
                sum += (hasMine(g, i, j) ? 1 : 0);
        sum -= (hasMine(g, r, c) ? 1 : 0);
        return sum;
    }

    private static boolean hasMine(Grid g, int r, int c) {
        Space sp = new Space(r, c);
        return isValid(g, sp) && g.getElementAt(sp) == g.MINE;
    }

    private static boolean isValid(Grid g, Space sp) {
        int r = sp.getRow(), c = sp.getCol();
        return r >= 0 && r < g.getRow() && c >= 0 && c < g.getCol();
    }
}
