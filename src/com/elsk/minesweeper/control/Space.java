
package com.elsk.minesweeper.control;

/**
 * An ordered pair of number that denotes a certain space in the grid.
 * It contains only the row and column number of the space.
 * This class facilitates returning position of spaces and comparing spaces 
 * (whether they are equal).
 * A <code>Space</code> is immutable once constructed.
 * It DOES NOT contain any information such as: whether there is a 
 * mine in this space (contained in  <code>minesweeper.Grid</code>); how many 
 * mines are around this space (contained in <code>minesweeper.Grid</code>); 
 * whether this space is undisclosed, disclosed, flagged, or question-marked 
 * (contained in <code>minesweeper.PlayBoard</code>). 
 * @author Jacky
 */
public class Space {
    private int row,col;

    public Space(int row, int col) {
        this.row = row;
        this.col = col;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + this.row;
        hash = 71 * hash + this.col;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Space other = (Space) obj;
        if (this.row != other.row) {
            return false;
        }
        if (this.col != other.col) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "[" + row + ", " + col + "]";
    }
    
}
