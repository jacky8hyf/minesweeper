
package com.elsk.minesweeper.control;

/**
 *
 * @author Jacky
 */
public class Time {
    private long h;
    private byte m, s;

    //this is the only method that allows to change h,m and s.
    private void setStandardizeTime(long h, long m, long s){
        if(h<0) throw new IllegalArgumentException("Illegal hour "+h);
        if(m<0) throw new IllegalArgumentException("Illegal minute "+m);
        if(s<0) throw new IllegalArgumentException("Illegal second "+s);
        if(s>=60){
            m+=s/60; s%=60;
        }
        if(m>=60){
            h+=m/60; m%=60;
        }
        this.h = h;
        this.m = (byte)m;
        this.s = (byte)s;
    }
    
    public Time(long h, long m, long s) {
        setStandardizeTime(h,m,s);
    }

    public Time(Time other) {
        copyTime(other);
    }
    
    public Time(long s){
        this(0,0,s);
//        standardize();
    }

    public Time() {
        this(0,0,0);
    }

    public long getH() {
        return h;
    }

    public byte getM() {
        return m;
    }

    public byte getS() {
        return s;
    }
    public long getTimeInSeconds(){
        return (long)h*3600+(long)m*60+s;
    }

    public void setH(long h) {
        setStandardizeTime(h, this.m, this.s);
    }

    public void setM(long m) {
        setStandardizeTime(this.h, m, this.s);
    }

    public void setS(long s) {
        setStandardizeTime(this.h, this.m, s);
    }
    
    public void copyTime(Time other){
        this.h = other.h;
        this.m = other.m;
        this.s = other.s;
//        standardize();
    }

    public void incrementSecond(){
        setStandardizeTime(this.h, this.m, this.s+1);
    }
//    public void standardize(){
//        if(s>=60){
//            m+=s/60; s%=60;
//        }
//        if(m>=60){
//            h+=m/60; m%=60;
//        }
//    }
//    public Time standardize(long h, long m, long s){
//        if(s>=60){
//            m+=s/60; s%=60;
//        }
//        if(m>=60){
//            h+=m/60; m%=60;
//        }
//        return new Time(h, m, s);
//    }
    @Override
    public String toString() {
        return  h + ":" + (m>=10?m:"0"+m) + ":" + (s>=10?s:"0"+s) ;
    }

    
}
