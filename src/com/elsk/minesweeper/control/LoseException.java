
package com.elsk.minesweeper.control;

/**
 *
 * @author Jacky
 */
public class LoseException extends GameStatusException {

    private Space sp;
    public LoseException(Space sp) {
        super("You lose by accidently clicked on"+sp+"!");
        this.sp = sp;
    }
    public Space getFinalSpace(){
        return sp;
    }
    
}
