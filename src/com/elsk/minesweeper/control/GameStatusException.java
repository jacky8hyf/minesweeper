
package com.elsk.minesweeper.control;

/**
 *
 * @author Jacky
 */
public class GameStatusException extends Exception{

    public GameStatusException(String message) {
        super(message);
    }
    
}
