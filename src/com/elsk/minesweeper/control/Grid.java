
package com.elsk.minesweeper.control;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

/**
 * Contains mainly the underlying grid of mines and numbers.
 * @author Jacky
 */
public class Grid {
    private byte [][] g;
    private int  row, col, numMine;
    public final static int LESS_GAP = 9;//mine number should be at least 9 less than row*col
    public final byte MINE=-1, EMPTY = 0;
    private HashSet<Space> myBlock;
    
    /**
     * Create a new mine grid with the specified number of rows
     * and columns, with mines randomly put in the grid;
     * @param row number of rows
     * @param col number of columns
     * @param numMine number of mines
     */
    public Grid(int row, int col, int numMine) {
        this(row, col, numMine, new Space(row, col)); 
            //g[row][column] is not valid, and thus empty anyway.
    }

    /**
     * Create a new mine grid with the specified number of rows
     * and columns, with mines randomly put in the grid and 
     * <code>firstClick</code> ensured blank. The <code>firstClick</code> is the
     * space of first click of the user, who does not want to be blown up in the 
     * beginning of the game.
     * @param row number of rows
     * @param col number of columns
     * @param numMine number of mines, <code>0 < numMine < row*col-1</code>
     * @param firstClick the space of the first click
     */
    public Grid(int row, int col, int numMine, Space firstClick) {
        //<editor-fold defaultstate="collapsed" desc="Obsolete: old Code">
        //        if(row<=0)throw new IllegalArgumentException("Invalid Row Number (require row>0)");
        //        if(col<=0) throw new IllegalArgumentException("Invalid Column Number (require column>0)");
        //        if(numMine<0 || numMine>row*col-1) throw new IllegalArgumentException("Invalid Mine Number (require 0<=numMine<=row*column-1");
        //        this.row = row;
        //        this.col = col;
        //        this.numMine = numMine;
        //        g = new byte[row][col];
        //        for(int i=0; i<row; i++)for(int j=0; j<col; j++)g[i][j] = EMPTY;
        //        Random rand = new Random();
        //        int i=0;
        //        //Put Mines
        //        while (i<numMine){
        //            int tempr = rand.nextInt(row), tempc = rand.nextInt(col);
        //            if(!hasMine(tempr, tempc)){  //if no mines in g[tempr][tempc]
        //                g[tempr][tempc]=MINE;   //put a mine
        //                i++;
        //            }
        //        }
        //        //Remove the mine on g[blankX][blankY] (if there is one there) and assign it somewhere else
        //        if(hasMine(firstClick)){
        //            Space newSpace = new Space(rand.nextInt(row), rand.nextInt(col));//find a random new place
        //            while(
        //                    (newSpace.equals(firstClick)) //it is still the original place
        //                    || hasMine(newSpace) //the new place has a mine already
        //                    )
        //            {
        //                 newSpace = new Space(rand.nextInt(row), rand.nextInt(col));//find a random new place again
        //            }
        //            g[firstClick.getRow()][firstClick.getCol()]=0;
        //            g[newSpace.getRow()][newSpace.getCol()]=MINE;
        //        }
        //        //Write the number of the surrounding mines in the blank places
        //        for(int j=0; j<row; j++) for(int k=0; k<col; k++)
        //            if(!hasMine(j, k))// if empty
        //                g[j][k] = sumSurroundingMines(new Space(j, k));
        //</editor-fold>
        if(row<=0)throw new IllegalArgumentException("Invalid Row Number (require row>0)");
        if(col<=0) throw new IllegalArgumentException("Invalid Column Number (require column>0)");
        if(numMine<0 || numMine>row*col-LESS_GAP) throw new IllegalArgumentException("Invalid Mine Number (require 0<=numMine<=row*column-"+LESS_GAP);
        this.row = row;
        this.col = col;
        this.numMine = numMine;
        g = new byte[row][col];
        for(int i=0; i<row; i++)for(int j=0; j<col; j++)g[i][j] = EMPTY;
        Random rand = new Random();
        int firstR = firstClick.getRow(),firstC = firstClick.getCol();
//      if((double)numMine/(row*col)<0.5){
        int i=0;
        //Put Mines
        while (i<numMine){
            int tempr = rand.nextInt(row), tempc = rand.nextInt(col);
            if(!hasMine(tempr, tempc) && 
                    !(Math.abs(firstR-tempr)<=1 && Math.abs(firstC-tempc)<=1)
                ){  //if no mines in g[tempr][tempc]
                g[tempr][tempc]=MINE;   //put a mine
                i++;
            }
        }
        //<editor-fold defaultstate="collapsed" desc="Obsolete: Another expected more efficient algorithm for large grid? Actually less efficient!">
            //        } else {
            //            int available = row*col-Grid.LESS_GAP;
            //            for(int i=0; i<numMine; i++) {
            //                putMineAtLinearPosition(g, firstR, firstC, rand.nextInt(available)+1); //1<=nextInt()<=available
            //                available--;
            //            }
            //        }
            //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="Obsolete: Remove the mine on g[blankX][blankY] (if there is one there) and assign it somewhere else">
            //        if(hasMine(firstClick)){
            //            Space newSpace = new Space(rand.nextInt(row), rand.nextInt(col));//find a random new place
            //            while(
            //                    (newSpace.equals(firstClick)) //it is still the original place
            //                    || hasMine(newSpace) //the new place has a mine already
            //                    )
            //            {
            //                 newSpace = new Space(rand.nextInt(row), rand.nextInt(col));//find a random new place again
            //            }
            //            g[firstClick.getRow()][firstClick.getCol()]=0;
            //            g[newSpace.getRow()][newSpace.getCol()]=MINE;
            //        }
            //</editor-fold>
        
            //Write the number of the surrounding mines in the blank places
        for(int j=0; j<row; j++) for(int k=0; k<col; k++) 
            if(!hasMine(j, k))// if empty
                g[j][k] = sumSurroundingMines(new Space(j, k));
    }
    
    ////<editor-fold defaultstate="collapsed" desc="putMineAtLinearPosition">
//    private void putMineAtLinearPosition(byte[][] g, int firstR, int firstC, int pos) {
        //        int i=0, r=0, c=-1, rowLength = g[0].length; //pos++;
        //        while(i<pos){
        //            c++;
        //            if(c>=rowLength){
        //                c=0; r++;
        //            }
        //            if(!hasMine(r,c) && !(Math.abs(firstR-r)<=1 && Math.abs(firstC-c)<=1) )
        //                i++;
        //        }
        ////        System.out.println(this.toString()+" "+ firstR+ " "+ firstC+ " "+ pos +" "+ r +" "+c);
        //        g[r][c]=MINE;
        //    }
        //</editor-fold>
    
    /**
     * Return the number at the specified space.
     * @param sp the specified space. Precondition: <code>isValid(sp)==true</code>.
     * @return the number at the space in the grid
     */
    public byte getElementAt(Space sp){
        return g[sp.getRow()][sp.getCol()];
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public int getNumMine() {
        return numMine;
    }
    
    /**
     * Return the block of empty spaces and its margin. This is normally
     * invoked when a space if uncovered.
     * @param sp the space from which the block is defined.
     * @return if <code>sp</code> has a number, only itself is returned; else, if it is equal
     * to EMPTY, the block of adjacent empty spaces and the margin spaces with
     * numbers would be returned.
     */
    public HashSet<Space> getBlockAround(Space sp){
        myBlock = new HashSet<Space>();
        addToBlock(sp);
        return myBlock;
    }
    
    /**
     * Recursion method in order to find the block.
     * @param sp the current space working on
     */
    private void addToBlock(Space sp){
        myBlock.add(sp);
        if(getElementAt(sp)==EMPTY){
            int r = sp.getRow(), c = sp.getCol();
            Space[] arr = {new Space(r-1,c), new Space(r+1,c), 
                new Space(r,c-1), new Space(r,c+1),
                new Space(r-1,c-1),new Space(r-1,c+1),
                new Space(r+1,c-1),new Space(r+1,c+1)};
            for(Space adjSp:arr)if(isValid(adjSp) && !myBlock.contains(adjSp)) addToBlock(adjSp);
        }
    }
    
    /**
     * Return the number of surrounding mines. 
     * There is not expected to be a mine in <code>sp</code>. If so, it
     * is not counted.
     * @param sp the specified space
     * @return The sum of the number of surrounding mines. 
     */
    private byte sumSurroundingMines(Space sp) {
        byte sum=0;
        int r = sp.getRow(), c = sp.getCol();
        for(int i=r-1;i<=r+1 ;i++ ) 
            for(int j=c-1; j<=c+1; j++) 
            //if(isValid(i,j)) 
                sum+=(hasMine(i,j)?1:0);
        sum-=(hasMine(r,c)?1:0);
        return sum;
    }
    
    /**
     * Return whether a space is out of range.
     * @param sp the specified space
     * @return whether the space is in the grid
     */
    private boolean isValid(Space sp) {
        int r = sp.getRow(), c = sp.getCol();
       return r>=0 && r<row && c>=0 && c<col;
    }
    
    /**
     * Return whether a space is occupied by a mine. If the specified space
     * is out of range (is not valid), return FALSE.
     * @param r row number
     * @param c column number
     * @return whether the space is empty
     */
    private boolean hasMine(int r,int c){
        return isValid(new Space(r,c)) && g[r][c]==MINE;
    }
    
    /**
     * Return whether a space is occupied by a mine. If the specified space
     * is out of range (is not valid), return FALSE.
     * @param sp the specified space
     * @return whether the space is empty
     */
    public boolean hasMine(Space sp){
        return isValid(sp) && getElementAt(sp)==MINE;
    }
    
    @Override
    public String toString() {
        String s = new String();
        s+="   |";
        for(int j=0; j<col; j++) s+= (j%10)+" "; 
        s+="\n---";
        for(int j=0; j<col; j++) s+="--";
        s+="\n";
        for(int i=0; i<row; i++) {
            s+=(i<10?" "+i:i%100)+" |";
            for(int j=0; j<col; j++)
                s+=(hasMine(i,j)?"*":(g[i][j]==EMPTY?"_":g[i][j]))+" ";
            s+="\n";
        }
        return s;
    }

}
