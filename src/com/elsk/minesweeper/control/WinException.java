
package com.elsk.minesweeper.control;

/**
 *
 * @author Jacky
 */
public class WinException extends GameStatusException  {

//    private Time timeLength;
//    public WinException(Time time) {
//        super("You win! Time: "+time);
//    }

    public WinException() {
        super("You win!");
//        timeLength = new Time(0,0,0);
    }
    
//    public Time getTimeLength(){
//        return timeLength;
//    }
    
}
