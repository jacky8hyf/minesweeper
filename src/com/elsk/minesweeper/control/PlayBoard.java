
package com.elsk.minesweeper.control;

import com.elsk.minesweeper.graphics.Skin;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

/**
 * Contains information the surface of the board, mainly whether a 
 * certain space is undisclosed, disclosed, flagged, or question-marked;
 * implements actions when certain spaces are left-clicked, right-
 * clicked, or left+right-clicked.
 * @author Jacky
 */
public class PlayBoard {
    private Grid grid;
    private byte status[][];
    public final byte UND = 0, //undisclosed
            DIS = 1, //disclosed
            FLG = 2, //flag
            QXN = 3; //question mark
    private int row, col, numMine, flagged, disclosed;

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }


    /**
     * Create a new MineSweeper play board without a specified mine grid.
     * Post Condition: this.grid==null.
     * Usually used when a new game is started.
     * @param row number of rows of the grid
     * @param column number of columns of the grid
     * @param numMine number of mines in the grid
     */
    public PlayBoard(int row, int column, int numMine) {
        startNewGame(row, column, numMine);
    }
    
    /**
     * Create a new MineSweeper play board with the existing mine grid 
     * with default parameters.
     * Usually used when a game is played again.
     */
    public PlayBoard() {
        this(10,10,10);
    }
    
    /**
     * Restart the game with the existing mine grid. If <code>grid==null</code>, a
     * new grid will be generated upon the first left click.
     */
    public void restart(){
        this.flagged = 0;
        this.disclosed = 0;
        this.status = new byte[row][col];
    }
    /**
     * Start a new game with the specified parameters.
     * @param row number of rows of the grid
     * @param column number of columns of the grid
     * @param numMine number of mines in the grid
     */
    public void startNewGame (int row, int column, int numMine) {
        this.row = row;
        this.col = column;
        this.numMine = numMine;
        startNewGame();
    }
    /**
     * Start a new game with the current parameters.
     */
    public void startNewGame(){
        this.grid = null;
        restart();
    }
    /**
     * Create a new grid. Usually invoked at the first click of a new game.
     * Usually if(this.grid==null) should be tested before invoking this method
     * because no new grid should be generated if the user is playing a game 
     * again.
     * @param sp The space where the user clicked at first. A mine would not
     * be placed in this space.
     */
    private void newGrid(Space sp){
        this.grid = new Grid(row, col, numMine, sp);
    }
    
    /**
     * Invoked when an undisclosed (not question-marked and not flagged)
     * space is clicked, or an adjacent disclosed space call for 
     * uncovering; disclose everything in the block  that the space 
     * belongs.
     * @param sp the clicked space; should be valid and undisclosed in advance.
     * @throws GameStatusException when the game loses or wins
     */
    public void leftClickOn(Space sp) throws GameStatusException{
        if(isValid(sp) && getStatusAt(sp)==UND){ //if sp is undisclosed
            if(grid==null) newGrid(sp);//create new grid for the first click
            if(grid.getElementAt(sp)==grid.MINE) //if a mine is uncovered, BOOM!
                lose(sp);
            else {
                HashSet<Space> block = grid.getBlockAround(sp);
                for(Space s : block) {
                    setStatusAt(s, DIS); //directly disclose everything in the block
//                    System.out.println(s+" disclosed");
                }
            }
        }
        judgeWin();
    }
    
    /**
     * Invoked when a disclosed space is left+right clicked. If the
     * surrounding flags are equal to the number in the space, it is equivalent
     * to left-clicking every undisclosed (and not flagged and not question-
     * marked) spaces.
     * @param sp the clicked space; should be valid and disclosed in advance.
     * @throws GameStatusException when the game loses or wins
     */
    public void leftRightClickOn(Space sp) throws GameStatusException{
        if(isValid(sp) && getStatusAt(sp)==DIS){
            if(grid.getElementAt(sp)==sumSurroundingFlags(sp)){
                HashSet<Space> hs = getSurroundingUndisclosed(sp);
                for(Space adjSp:hs)leftClickOn(adjSp);
            }
        }
    }

    /**
     * Invoked when a undisclosed (blank, flagged, or question-marked)
     * space is right clicked. The status of the space will rotate among <code>UND</code>, 
     * <code>FLG</code>, and <code>QXN</code>.
     * @param sp the clicked space; should be valid and undisclosed in advance.
     */
    public void rightClickOn(Space sp) {
        if(isValid(sp)){
            byte st = getStatusAt(sp);
            switch(st){
                case UND: setStatusAt(sp,FLG); break;
                case FLG: setStatusAt(sp,QXN); break;
                case QXN: setStatusAt(sp,UND); break;
            }
        }
    }
    
    public int getRemainingMine(){
        return numMine-flagged;
    }
    
//    /**
//     * Refreshes the GUI after changing status of a certain space.
//     * @param sp the specified space to redraw
//     */
//    private void refreshSpace(Space sp){
////        throw new UnsupportedOperationException("Not yet implemented");
//    }
    
    /**
     * Return the status at the specified space.
     * @param sp the specified space. Precondition: <code>sp</code> is valid.
     * @return the status denoted by a byte, which is either <code>DIS</code>, <code>UND</code>, <code>FLG</code>, 
     * or <code>QXN</code>.
     */
    public byte getStatusAt(Space sp) {
        return status[sp.getRow()][sp.getCol()];
    }
    
    /**
     * Return the number of mines shown in the specified space.
     * @param sp The specified space
     * @return The number of mines shown in the specified space. If the 
     * specified space has a mine in <code>sp</code>, return 0.
     */
    public byte getSurroundingNumMine(Space sp){
        byte temp = grid.getElementAt(sp);
        return temp!=grid.MINE?temp:0;
    }
    public boolean hasMine (Space sp){
        return grid.hasMine(sp);
    }
    
    public HashSet<Space> getSurroundingUndisclosed(Space sp){
        int x=sp.getRow(), y = sp.getCol();
        HashSet<Space> hs = new HashSet();
        for(int i=x-1; i<=x+1; i++) for(int j=y-1; j<=y+1; j++){
            Space adjSp = new Space(i,j);
            if(isValid(adjSp) && getStatusAt(adjSp)==UND)hs.add(adjSp);
        }
        if(hs.contains(sp))hs.remove(sp);
        return hs;
    }
    /**
     * Set the status at the specified space to be a certain value.
     * @param sp the specified space. Precondition: <code>sp</code> is valid.
     * @param st the status denoted by a byte, which is either <code>DIS</code>, <code>UND</code>, <code>FLG</code>, 
     * or <code>QXN</code>.
     */
    private void setStatusAt(Space sp, byte st){
        if(status[sp.getRow()][sp.getCol()]==FLG) flagged--;
        else if (status[sp.getRow()][sp.getCol()]==DIS) disclosed--;
        status[sp.getRow()][sp.getCol()] = st;
        if(st==FLG) flagged++;
        else if (st==DIS) disclosed++;
//        refreshSpace(sp);
    }
    
    /**
     * Return the number of surrounding flags. 
     * There is not expected to be a flag in <code>status[i][j]</code>, since 
     * only undisclosed and unmarked spaces can be clicked. However, if so, this
     * is not counted.
     * @param the center space
     * @return The sum of the number of surrounding mines. 
     */
    private byte sumSurroundingFlags(Space sp) {
        byte sum=0;
        int x = sp.getRow(), y = sp.getCol();
        for(int i=x-1;i<=x+1 ;i++ ) 
            for(int j=y-1; j<=y+1; j++) 
                if(isValid(new Space(i,j))) 
                    sum+=status[i][j]==FLG?1:0;
        sum-=status[x][y]==FLG?1:0;
        return sum;
    }
    
    /**
     * Return whether a space is out of range.
     * @param sp the specified space
     * @return whether the space is in the grid
     */
    private boolean isValid(Space sp) {
        int x = sp.getRow(), y = sp.getCol();
        return x>=0 && x<row && y>=0 && y<col;
    }

    private void judgeWin() throws WinException {
        if(disclosed==row*col-numMine) throw new WinException();
//        throw new UnsupportedOperationException("Not yet implemented");
    }
    
    private void lose(Space sp) throws LoseException{
        discloseAllUnflaggedMines();
        throw new LoseException(sp);
    }
    
    @Override
    public String toString() {
        String s = new String();
        s+="Mine Remaining: "+this.getRemainingMine()+"\n";
        s+="   |";
        for(int j=0; j<col; j++) s+= (j%10)+" "; 
        s+="\n---+";
        for(int j=0; j<col; j++) s+="--";
        s+="\n";
        for(int i=0; i<row; i++) {
            s+=(i<10?" "+i:i%100)+" |";
            for(int j=0; j<col; j++){
                if(status[i][j]==DIS){
                    int ele = grid.getElementAt(new Space(i,j));
                    s+= ele==grid.MINE?Skin.MINECHAR:ele==grid.EMPTY?Skin.DISCHAR:ele;
                }
                else{
                    int st = status[i][j];
                    s+= st==UND?Skin.UNDCHAR:
                        st==FLG?Skin.FLGCHAR:
                        st==QXN?Skin.QXNCHAR:
                        "";
                }
                s+=" ";
            }
            s+="\n";
        }
        return s;
    }

    private void discloseAllUnflaggedMines() {
        for(int i=0; i<row; i++) for(int j=0; j<col; j++){
            Space sp = new Space(i,j);
            if(grid.hasMine(sp) && !(status[i][j]==FLG))setStatusAt(sp,DIS);
        }
    }
}
