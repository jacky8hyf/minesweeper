
package com.elsk.minesweeper.graphics;

import com.elsk.minesweeper.control.Time;

/**
 *
 * @author Jacky
 */
public class Timer extends java.util.Timer {

    private long startTime;

    /**
     * Create a new Timer and start it.
     */
    public Timer() {
        restart();
    }

    /**
     * Return the elapsed time since the construction of the object.
     * @return 
     */
    public Time getElapsedTime() {
        long nowTime = System.currentTimeMillis();
//        System.out.println("getElapsedTime()"+(nowTime-startTime)/1000l);
        return new Time((nowTime-startTime)/1000l);
    }
    public void restart(){
        this.startTime = System.currentTimeMillis();
    }
}
