/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elsk.minesweeper.graphics;

import com.elsk.minesweeper.control.Time;
import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

/**
 *
 * @author Jacky
 */
public class MainPanel extends javax.swing.JPanel {

    private java.util.Timer refreshTimer;
//    private RefreshTimeTask task;
    private static MainPanel globalMainPanel;
    private static JFrame frame;
    private com.elsk.minesweeper.graphics.Timer myTimer;

    public static MainPanel getGlobalMainPanel() {
        return globalMainPanel;
    }

    public MainPanel() {
        this(null);
    }

    /**
     * Creates new form MainPanel.
     */
    public MainPanel(String skinName) {
        MainPanel.globalMainPanel = this;
        if (skinName != null) {
            new Skin(skinName);
        } else {
            new Skin();
        }
        //<editor-fold defaultstate="collapsed" desc="Obsolete: Init sub-panels manually">
        //        gameStatusPanel = new GameStatusPanel();
        //        playBoardPanel = new PlayBoardPanel(this);
        //        setBackground(new java.awt.Color(200, 200, 200));
        //</editor-fold>
        initComponents();
//        this.gameStatusPanel.setBackground(Color.GRAY);
//        this.playBoardPanel.setBackground(Color.GRAY);
//        this.setMinimumSize(new java.awt.Dimension(this.gameStatusPanel.getPreferredSize().width,0));
        int[] arr = LevelChooser.EASY; //DEFAULT GAME BOARD
        startNewGame(arr[0], arr[1], arr[2]);
//        gameStatusPanel.showLevelChooserDialog();
//        gameStatusPanel.
        showRemainingMine();
        resetTime();
        //<editor-fold defaultstate="collapsed" desc="Obsolete: Original initComponents">
//        javax.swing.GroupLayout gameStatusPanelLayout = new javax.swing.GroupLayout(gameStatusPanel);
//        gameStatusPanel.setLayout(gameStatusPanelLayout);
//        gameStatusPanelLayout.setHorizontalGroup(
//            gameStatusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
//            .addGap(0, 0, Short.MAX_VALUE)
//        );
//        gameStatusPanelLayout.setVerticalGroup(
//            gameStatusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
//            .addGap(0, 100, Short.MAX_VALUE)
//        );
//
//        javax.swing.GroupLayout playBoardPanelLayout = new javax.swing.GroupLayout(playBoardPanel);
//        playBoardPanel.setLayout(playBoardPanelLayout);
//        playBoardPanelLayout.setHorizontalGroup(
//            playBoardPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
//            .addGap(0, playBoardPanel.getPreferredSize().width, Short.MAX_VALUE)
//        );
//        playBoardPanelLayout.setVerticalGroup(
//            playBoardPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
//            .addGap(0, playBoardPanel.getPreferredSize().height, Short.MAX_VALUE)
//        );
//
//        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
//        this.setLayout(layout);
//        layout.setHorizontalGroup(
//            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
//            .addGroup(layout.createSequentialGroup()
//                .addContainerGap()
//                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
//                    .addComponent(gameStatusPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
//                    .addComponent(playBoardPanel, javax.swing.GroupLayout.DEFAULT_SIZE, playBoardPanel.getPreferredSize().width, Short.MAX_VALUE))
//                .addContainerGap())
//        );
//        layout.setVerticalGroup(
//            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
//            .addGroup(layout.createSequentialGroup()
//                .addContainerGap()
//                .addComponent(gameStatusPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
//                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
//                .addComponent(playBoardPanel, javax.swing.GroupLayout.DEFAULT_SIZE, playBoardPanel.getPreferredSize().height, Short.MAX_VALUE)
//                .addContainerGap())
//        );
//                //</editor-fold>
    }

    void showRemainingMine() {
        gameStatusPanel.showRemainingMine(this.playBoardPanel.getRemainingMine());
    }

    //TIME
    void startNewTimer() {
//        RefreshTimeTask task = new RefreshTimeTask();
        if (myTimer != null) {
            myTimer.restart();
        } else {
            myTimer = new com.elsk.minesweeper.graphics.Timer();//Timer begins
        }
        if (refreshTimer != null) {
            refreshTimer.cancel(); //stops former refresher
        }
        refreshTimer = new java.util.Timer();
        refreshTimer.schedule(new RefreshTimeTask(), 0, 1000); //starts new refresher
    }

    private void resetTime() {
        gameStatusPanel.showTime(new Time()); //stops the time
        if (refreshTimer != null) {
            refreshTimer.cancel(); //stops the refresher
        }
        refreshTimer = null;
    }

    //GAME CONTROL
    void startNewGame(int row, int col, int numMine) {
        playBoardPanel.startNewGame(row, col, numMine);
        showRemainingMine();
        resetTime();
        resize();
//        startNewTimer();
    }

    void startNewGame() {
        playBoardPanel.startNewGame();
        showRemainingMine();
        resetTime();
    }

    void restartGame() {
        playBoardPanel.restartGame();
        showRemainingMine();
        resetTime();
//        resize();
//        startNewTimer();
    }

    void win() {
//        repaint();
        refreshTimer.cancel();
        JOptionPane.showMessageDialog(null, "You Win! Time:" + myTimer.getElapsedTime(), "Congratulations!", JOptionPane.INFORMATION_MESSAGE);
    }

    void lose() {
//        repaint();
        refreshTimer.cancel();
        JOptionPane.showMessageDialog(null, "You Lose!", "Sorry...", JOptionPane.INFORMATION_MESSAGE);
    }

    //UI CONTROL
    /**
     * Adjust to the appropriate size according to the preferred sizes of
     * sub-panels. The preferred sizes often stores the ideal sizes of the
     * sub-panels.
     */
    private void resize() {
        //<editor-fold defaultstate="collapsed" desc="comment">
        //        System.out.println("Before MainPanel.resize(): m.size:"+this.getSize()+" pbp.size:"+this.playBoardPanel.getSize()+" pbp.pref:"+this.playBoardPanel.getPreferredSize());
        //        java.awt.Dimension pbsize = playBoardPanel.getSize(), gspsize = gameStatusPanel.getSize(),
        //                framesize = frame.getSize(), zero = new java.awt.Dimension();
        //        pbsize = zero.equals(pbsize)?this.playBoardPanel.getPreferredSize():pbsize;
        //java.awt.Dimension pbsize = playBoardPanel.getPreferredSize(), gspsize = gameStatusPanel.getPreferredSize(), zero = new java.awt.Dimension();
        //        gspsize = zero.equals(gspsize)?this.gameStatusPanel.getPreferredSize():gspsize;
        //</editor-fold>
        java.awt.Dimension pbPrefSize = playBoardPanel.getPreferredSize(),
                gspPrefSize = gameStatusPanel.getPreferredSize(),
                pbSize = playBoardPanel.getSize(),
                gspSize = gameStatusPanel.getSize(),
                framesize = frame.getSize();
//       System.out.println(gspPrefSize);
        //The difference is found by the current sizes.
        int widthgap = framesize.width - Math.max(pbSize.width, gspSize.width),
                heightgap = framesize.height - pbSize.height - gspSize.height;

        frame.setSize(new java.awt.Dimension(Math.max(pbPrefSize.width, gspPrefSize.width) + widthgap, gspPrefSize.height + pbPrefSize.height + heightgap));
        //<editor-fold defaultstate="collapsed" desc="comment">
        //        System.out.println("framesize:"+frame.getSize());
        //          this.setPreferredSize(new java.awt.Dimension(pbsize.width,gspsize.height + pbsize.height
        //                +(this.getHeight()-this.playBoardPanel.getHeight()-this.gameStatusPanel.getHeight())));
        //the gap between the two panels,==6
        //        frame.setSize(this.getPreferredSize().width+widthgap,this.getPreferredSize().height+heightgap);
        //        System.out.println("After MainPanel.resize(): m.size:"+this.getSize()+" pbp.size:"+this.playBoardPanel.getSize()+" pbp.pref:"+this.playBoardPanel.getPreferredSize());

        //        System.out.println("Main:"+this.getSize());
        //</editor-fold>
    }

    public static void main(String[] args) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
            /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */

        for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
            if ("Nimbus".equals(info.getName())) {
                try {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, ex.getMessage(), "Exception Occur!", JOptionPane.ERROR_MESSAGE);
                }
            }
        }

        //</editor-fold>
        frame = new JFrame("Elsk Minesweeper");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        if (args.length > 0) {
            frame.add(new MainPanel(args[0]));
        } else {
            frame.add(new MainPanel());
        }
        frame.pack();
        frame.setVisible(true);
    }

    /**
     * This method is called from within the constructor to initialize the
     * form. WARNING: Do NOT modify this code. The content of this method
     * is always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        playBoardPanel = new com.elsk.minesweeper.graphics.PlayBoardPanel();
        gameStatusPanel = new com.elsk.minesweeper.graphics.GameStatusPanel();

        setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout playBoardPanelLayout = new javax.swing.GroupLayout(playBoardPanel);
        playBoardPanel.setLayout(playBoardPanelLayout);
        playBoardPanelLayout.setHorizontalGroup(
            playBoardPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 608, Short.MAX_VALUE)
        );
        playBoardPanelLayout.setVerticalGroup(
            playBoardPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 340, Short.MAX_VALUE)
        );

        gameStatusPanel.setPreferredSize(new java.awt.Dimension(400, 80));

        javax.swing.GroupLayout gameStatusPanelLayout = new javax.swing.GroupLayout(gameStatusPanel);
        gameStatusPanel.setLayout(gameStatusPanelLayout);
        gameStatusPanelLayout.setHorizontalGroup(
            gameStatusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        gameStatusPanelLayout.setVerticalGroup(
            gameStatusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 80, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(gameStatusPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 608, Short.MAX_VALUE)
            .addComponent(playBoardPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(gameStatusPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(playBoardPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.elsk.minesweeper.graphics.GameStatusPanel gameStatusPanel;
    private com.elsk.minesweeper.graphics.PlayBoardPanel playBoardPanel;
    // End of variables declaration//GEN-END:variables

    private class RefreshTimeTask extends java.util.TimerTask {
//        private com.elsk.minesweeper.graphics.Timer myTimer;

//        public RefreshTimeTask() {
////            myTimer = new com.elsk.minesweeper.graphics.Timer();
//        }
        @Override
        public void run() {
            gameStatusPanel.showTime(myTimer.getElapsedTime());
        }
//        private Time getElapsedTime(){
//            return myTimer.getElapsedTime();
//        }
    }
}
