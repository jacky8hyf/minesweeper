
package com.elsk.minesweeper.graphics;

/**
 *
 * @author Jacky
 */
public class InvalidClickPositionException extends Exception {

    public InvalidClickPositionException() {
        super("Click Position is Invalid");
    }

    public InvalidClickPositionException(int x, int y) {
        super("Click Position is Invalid:["+x+", "+y+"]");
    }
    
}
