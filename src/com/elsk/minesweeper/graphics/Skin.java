
package com.elsk.minesweeper.graphics;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author Jacky
 */
public class Skin {
    private static final int IMGNUM=11;
    private static String 
                         imgPath[] = new String[IMGNUM],
                         defaultSkinName = "default",
                         skinParentPath = "",
                         numImgPath[] = new String[11], //numImgPath[10] for negative sign
                         fileNames[] = {
                             "undisclosed",//0
                             "disclosed",//1
                             "flag",//2
                             "question_mark",//3
                             "pressing",//4
                             "blank",//5
                             "mine",//6
//                             "product",//7
                             "colon",//7
                             "startnew",//8
                             "restart",//9
                             "level"//10
                         };
    static{
        for(int i=0; i<fileNames.length; i++)
            fileNames[i]+=".png";
    }
    private static BufferedImage img[] = new BufferedImage[IMGNUM],
                                numImg[] = new BufferedImage[numImgPath.length];

    private static final char NUMCHAR[] = new char[10];
    public static final char
            MINECHAR = '*',
            FLGCHAR = 'P',
            QXNCHAR = '?',
            UNDCHAR = 'M',
            DISCHAR = '_',
            PRSCHAR = 'W',
            BLANKCHAR = ' ',
            PRODUCTCHAR = 'X',
            COLONCHAR = ':',
            NEGCHAR = '-';
    static{
        for(int i=0; i<NUMCHAR.length; i++)NUMCHAR[i]=(char)(i+'0');
    }

    public static BufferedImage getStartNewImg() {
        return img[8];
    }

    public static BufferedImage getRestartImg() {
        return img[9];
    }

    public static BufferedImage getLevelImg() {
        return img[10];
    }

    public static BufferedImage getImage(char code){
        if(code>=NUMCHAR[0] && code<=NUMCHAR[9]) return numImg[code-'0'];
        switch(code){
            case MINECHAR: return img[6];
            case FLGCHAR: return img[2];
            case QXNCHAR: return img[3];
            case UNDCHAR: return img[0];
            case DISCHAR: return img[1];
            case PRSCHAR: return img[4];
            case BLANKCHAR: return img[5];
//            case PRODUCTCHAR: return img[7];
//            case COLONCHAR: return img[8];
            case COLONCHAR: return img[7];
            case NEGCHAR: return numImg[10];
        }
        throw new IllegalArgumentException();
    }

    public static BufferedImage getMineNumImg(int mineNum){
        if(mineNum==0)return img[5];
        return numImg[mineNum]; //0<=mineNum<=8
    }
    
    public static BufferedImage getNumImg(int num){
            return numImg[num]; //0<=num<=9
    }
    
    public static int getNumImgHeight(){
        return numImg[0].getHeight();
    }

    public static int getSpaceImgHeight(){
        return img[0].getHeight();
    }
    
    public Skin() {
        setDefaultSkin();
    }
    
    public Skin(String skinName){
        setSkin(skinName);
    }
    
    private void init(String skinName) throws IOException{
        try {
            String skinPath = Skin.skinParentPath+skinName+"/";
            for(int i=0; i<IMGNUM; i++)imgPath[i] = skinPath + fileNames[i];

            for(int i=0; i<Skin.numImgPath.length-1; i++) 
                Skin.numImgPath[i] = skinPath+"num_"+i+".png";
            Skin.numImgPath[10] = skinPath+"num_negative.png";
            for(int i=0; i<IMGNUM; i++)
//                if(Skin.img[i]==null) 
                    Skin.img[i] = ImageIO.read(getClass().getResource(Skin.imgPath[i]));

            for(int i=0; i<Skin.numImg.length; i++)
//                if(Skin.numImg[i] == null) 
                    Skin.numImg[i] = ImageIO.read(getClass().getResource(Skin.numImgPath[i]));
        } catch (Exception ex) {
            String skinPath = skinName+"/";
            for(int i=0; i<IMGNUM; i++)imgPath[i] = skinPath + fileNames[i];

            for(int i=0; i<Skin.numImgPath.length-1; i++) 
                Skin.numImgPath[i] = skinPath+"num_"+i+".png";
            Skin.numImgPath[10] = skinPath+"num_negative.png";
            for(int i=0; i<IMGNUM; i++)
//                if(Skin.img[i]==null) 
                    Skin.img[i] = ImageIO.read(new File(Skin.imgPath[i]));

            for(int i=0; i<Skin.numImg.length; i++)
                if(Skin.numImg[i] == null) 
                    Skin.numImg[i] = ImageIO.read(new File(Skin.numImgPath[i]));
        }
    }
    
    private void refresh(String skinName) throws IOException{
//        if(img!=null)for(int i=0; i<img.length; i++)img[i]=null;
//        if(numImg!=null)for(int i=0; i<numImg.length; i++)numImg[i]=null;
        init(skinName);
    }
    
    private void setSkin(String skinName){
        try{
            refresh(skinName);
        }
        catch (IOException ex){
            System.err.println("Skin "+skinName+" could not be read! Use default skin.");
//            ex.printStackTrace();
            setDefaultSkin();
        }
        catch (IllegalArgumentException ex){
            System.err.println("Skin "+skinName+" is not found! Use default skin.");
            setDefaultSkin();
            }
        }
    
    
    private void setDefaultSkin(){
        try{
            refresh(Skin.defaultSkinName);
        }catch (IOException ex){
            System.err.println("Default skin could not be read! Program will exit.");
            System.exit(1);
        }catch (IllegalArgumentException ex){
            System.err.println("Default skin is not found! Program will exit.");
            System.exit(1);
        }
    }
    

}
