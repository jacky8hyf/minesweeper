
package com.elsk.minesweeper.graphics;

import com.elsk.minesweeper.control.GameStatusException;
import com.elsk.minesweeper.control.LoseException;
import com.elsk.minesweeper.control.PlayBoard;
import com.elsk.minesweeper.control.Space;
import com.elsk.minesweeper.control.WinException;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.HashSet;

/**
 *
 * @author Jacky
 */
public class PlayBoardPanel extends javax.swing.JPanel {

    private static int space_size,num_img_size;
    private PlayBoard board;
    private boolean leftPressing,rightPressing,isLeftRightClick;
    private HashSet<Space> pressingList;
    private Space pressingSpace;
    private MainPanel mainPanel;
    private Dimension preferredSize;
    private boolean gameStarted, gameStopped;

    //before begin: gameStarted=false, gameStopped=false
    //after begin, before ending: gameStarted=true, gameStopped=false
    //after ending: gameStarted=true, gameStopped=true
    /**
     * Creates new form MainPanel
     */
    public PlayBoardPanel() {
//        BufferedImage disclosedSpaceImg=null;
//        try {
//            System.out.println(getClass());
//             disclosedSpaceImg = ImageIO.read(getClass().getResource("cartoon/disclosed.png"));
//        } catch (IOException ex) {
//           
//        }System.out.println(disclosedSpaceImg);
//        Resource r = new Resource();
       this.mainPanel = MainPanel.getGlobalMainPanel();
       space_size=30;
       num_img_size=20;
       leftPressing=false;
       rightPressing=false;
       isLeftRightClick=false;
       pressingList = new HashSet();
       pressingSpace = null;
//       board=new PlayBoard();
//       startNewGame(13,15,10);
//       showRemainingMine();
       
       initComponents();
//       startNewGame(10,10,10);
    }

     int getRemainingMine(){
//        int numMine = board.getRemainingMine();
//        System.out.println(gameStarted+" "+board.getRemainingMine()+board.getTotalMine());
        return board.getRemainingMine();
    }
    
    //GAME CONTROL
    void startNewGame(int row, int col, int numMine){
        if(board!=null)board.startNewGame(row, col, numMine);
        else board = new PlayBoard(row, col, numMine);
        gameStarted = gameStopped = false; //game starts upon first click.
        resize();        //the row and col in board changes, so resize() should be invoked.
    }
    /**
     * Starts a new game with existing parameters. Precondition: <code>board</code>
     * is defined.
     */
    void startNewGame(){
        board.startNewGame();
        gameStarted = gameStopped = false; //game starts upon first click.
        repaint();
    }
    
    void restartGame(){
        board.restart();
        gameStarted = gameStopped = false; //game starts upon first click.
        this.repaint();
    }
    
    //UI CONTROL
    /**
     * Adjust to the preferred size according to <code>space_size</code>.
     * The preferred size will be set to the ideal size of the panel with the 
     * current <code>space_size</code>. It is expected to be invoked if one of
     * the following changes:
     * 1. The row and column number of the board, usually upon the start of a
     * new game;
     * 2. <code>space_size</code>.
     * In particular, if space_size is not change, the visual scale of the board
     * will be kept as expected.
     * However, it does not change the size of the panel directly; it merely
     * stores it in the preferred size field.
     */
    private void resize(){
    //        int temp_width = Math.max(this.getWidth(), board.getCol()*space_size+2*space_size),
    //        temp_height = Math.max(this.getHeight(), board.getRow()*space_size+2*space_size);
//            System.out.println("Before PBP.resize(): size:"+this.getSize()+" pref:"+this.getPreferredSize());
    
            preferredSize = new Dimension(board.getCol()*space_size+2*space_size,
                    board.getRow()*space_size+2*space_size);
            this.setPreferredSize(preferredSize);
            repaint();
//            System.out.println("After PBP.resize(): size:"+this.getSize()+" pref:"+this.getPreferredSize());
    //        this.setSize(this.getPreferredSize());
        }
    
    /**
     * Adjust the <code>space_size</code> according to the current size of the panel.
     * That means it does not change the current size of the panel, but tries to
     * rescale the board to fit in the current size.
     */
    private void adjustSpaceSize(){
//                System.out.println("Before PBP.formComponentResized(): spsize:"+space_size+" size:"+this.getSize());
        this.space_size = Math.min(Math.min(this.getSize().width/(this.board.getCol()+2),
                this.getSize().height/(this.board.getRow()+2)),Skin.getSpaceImgHeight());
            this.num_img_size = Math.min(space_size
                *2/3
                , Skin.getNumImgHeight());
        resize();
                //2 for the surrounding gap
//        resize();
//        System.out.println("After PBP.formComponentResized(): spsize"+space_size+" size:"+this.getSize());
//        repaint();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
//        System.out.println("Paint");
//        System.out.println(grid_bfm);
        drawGrid(g, new Point(this.getWidth()/2,this.getHeight()/2));
        }
   
    /**
     * Draw a grid based on <code>board</code> which centers at a specified point.
     * If <code>board</code> is null, a <code>NullPointerException</code> will be thrown.
     * @param g The graphics object to draw on
     * @param center The coordinates of the center of the grid
     */
    private void drawGrid(Graphics g, Point center){
       for(int i=0; i<board.getRow(); i++) for(int j=0; j<board.getCol(); j++) {
//      Original Code: g.drawImage(space_bfm, space_size*(i*2-col)/2+center.x,space_size*(j*2-row)/2+center.y,space_size, space_size, null);
//      Original Code: drawImageCenterOnPoint(g,new Point(center.x+((2*i-col+1)*space_size/2),center.y+((2*j-row+1)*space_size/2)),disclosed_space_img);
            Space sp = new Space(i,j);
            byte status = board.getStatusAt(sp);
//            System.out.println(board);

//                System.out.println(sp);
            if(status==board.DIS){
                drawImageInSpace(g, center,sp, Skin.getImage(Skin.DISCHAR),space_size, space_size);
                if(board.hasMine(sp))
                    drawImageInSpace(g, center,sp, Skin.getImage(Skin.MINECHAR),num_img_size,num_img_size);
                else drawImageInSpace(g, center,sp, Skin.getMineNumImg(board.getSurroundingNumMine(sp)),num_img_size,num_img_size);
            } else {
                drawImageInSpace(g, center,sp, Skin.getImage(Skin.UNDCHAR), space_size, space_size);
                if(status==board.QXN)
                    drawImageInSpace(g, center,sp, Skin.getImage(Skin.QXNCHAR), num_img_size, num_img_size);
                else if(status==board.FLG)
                    drawImageInSpace(g, center, sp, Skin.getImage(Skin.FLGCHAR),num_img_size,num_img_size);
            }
        }
       //if(pressingList!=null)
//       System.out.println(pressingList);
        for (Space pressingSp : pressingList){
            drawImageInSpace(g,center, pressingSp, Skin.getImage(Skin.PRSCHAR), space_size,space_size);
        }
    }
    
    /**
     * Draw a certain image in a certain space based on the definition 
     * of <code>board</code>.
     * @param g The graphics object to draw on
     * @param grid_center The coordinates of the center of the grid
     * @param sp The specified space
     * @param img The image being drawn
     */
    private void drawImageInSpace(Graphics g, Point grid_center, Space sp, 
            BufferedImage img, int width, int height){
        drawImageCenterOnPoint(g,
                new Point(
                    grid_center.x+((2*sp.getCol()-board.getCol()+1)*space_size/2),
                    grid_center.y+((2*sp.getRow()-board.getRow()+1)*space_size/2)),
                img,
                width, height);
    }
    
    /**
     * Draw a certain image center on a certain point.
     * @param g The graphics object to draw on
     * @param center The coordinates of the center of the image
     * @param img The image being drawn
     * @param width The width of the image, will compress or stretch the image
     * @param height The height of the image, will compress or stretch the image
     */
    private void drawImageCenterOnPoint(Graphics g, Point center, BufferedImage img, int width, int height){
        g.drawImage(img, center.x-width/2, center.y-height/2, width, height, null);
    }
    
    //CLICK EVENT
    private Space getClickedSpace(Point p) throws InvalidClickPositionException{
        Point center = new Point(this.getWidth()/2, this.getHeight()/2);
        int tempy = (p.y-(center.y-space_size*this.board.getRow()/2)),
            tempx = (p.x-(center.x-space_size*this.board.getCol()/2));
        if(tempy<0 || tempx<0) throw new InvalidClickPositionException(p.x,p.y);
        Space sp = new Space(tempy/space_size, tempx/space_size);
        if(
                sp.getCol()>=board.getCol() || 
                sp.getRow()>=board.getRow())
            throw new InvalidClickPositionException(p.x,p.y);
        return sp;
    }
    
    private void leftClicked(Space sp) throws GameStatusException{
        board.leftClickOn(sp);
    }
    
    private void rightClicked(Space sp){
        board.rightClickOn(sp);
        mainPanel.showRemainingMine();
    }
    
    private void leftRightClicked(Space sp) throws GameStatusException{
        board.leftRightClickOn(sp);
    }
    
    private void updatePressingList(Point clickedPoint){
        if(clickedPoint == null){
            pressingList.clear();
        }
        else if(leftPressing || rightPressing){
            try {
                pressingSpace = this.getClickedSpace(clickedPoint);
                if(leftPressing && rightPressing) {
                    this.isLeftRightClick = true;
                    this.pressingList = board.getSurroundingUndisclosed(pressingSpace);
                } else {
                    this.pressingList.clear();
                    if(board.getStatusAt(pressingSpace)==board.UND) 
                        this.pressingList.add(pressingSpace);
                }
            } catch (InvalidClickPositionException ex) {
                pressingSpace = null;
            }
        }
        repaint();
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setBackground(new java.awt.Color(255, 255, 255));
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                formMouseReleased(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                formMousePressed(evt);
            }
        });
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                formComponentResized(evt);
            }
        });
        addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                formMouseDragged(evt);
            }
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                formMouseMoved(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void formMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMousePressed
//        try {
//            Space sp = getClickedSpace(evt.getPoint());
//            System.out.println("row="+sp.getRow()+" col="+sp.getCol());
//        } catch (InvalidClickPositionException ex) {
//            System.out.println(ex.getMessage());
//        }
        if(!gameStopped){ //before the game ends, either before or after the game begins 
            if(evt.getButton()==MouseEvent.BUTTON1) this.leftPressing = true;
            else if(evt.getButton()==MouseEvent.BUTTON3) this.rightPressing = true;
            updatePressingList(evt.getPoint());
                repaint();
        }
    }//GEN-LAST:event_formMousePressed

    private void formMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseReleased
        if(evt.getButton()==MouseEvent.BUTTON1 || evt.getButton()==MouseEvent.BUTTON3){
            if(!gameStarted && !gameStopped){ //before begin
                mainPanel.startNewTimer();
                gameStarted=true;            //begin the game
            }
            if(gameStarted && !gameStopped){ //game began
                if(evt.getButton()==MouseEvent.BUTTON1) this.leftPressing = false;
                else this.rightPressing = false;
                try {
                    if(pressingSpace!=null)pressingSpace = getClickedSpace(evt.getPoint());
                    if(!this.isLeftRightClick){
                        updatePressingList(null);
                        if(evt.getButton()==MouseEvent.BUTTON1) leftClicked(pressingSpace);
                        else if(evt.getButton()==MouseEvent.BUTTON3) rightClicked(pressingSpace);
                    } else {
                        if(!leftPressing && !rightPressing){
                            updatePressingList(null);
                            leftRightClicked(pressingSpace);
                            isLeftRightClick=false;
                        }
                    }
        //            System.out.println("row="+sp.getRow()+" col="+sp.getCol());
                } catch (InvalidClickPositionException ex) {
                    pressingSpace = null;
                } catch (NullPointerException ex){
                } catch (GameStatusException ex) {
                    if(ex instanceof WinException) win();
                    else if(ex instanceof LoseException) lose();
                    else ex.printStackTrace();
                }
            }
//            repaint();
        }
    }//GEN-LAST:event_formMouseReleased

    private void formMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseMoved
//        System.out.println("Moved: "+evt.getPoint());
//        generatePressingList(evt.getPoint());
    }//GEN-LAST:event_formMouseMoved

    private void formMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseDragged
//        System.out.println("Dragged: "+evt.getPoint());
        Space sp;
        if(leftPressing || rightPressing){
        try {
            sp = this.getClickedSpace(evt.getPoint());  
            } catch (InvalidClickPositionException ex) {
                sp = null;
            }
            if(pressingSpace!=null && !pressingSpace.equals(sp)){
                pressingSpace = null;
                leftPressing = rightPressing = false;
                updatePressingList(null);
                }
        }
//        if(leftPressing || rightPressing){
//            generatePressingList(evt.getPoint());
//        }
    }//GEN-LAST:event_formMouseDragged

    private void formComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentResized
        //repaint() will be called before calling this. So resize(), a repaint()
        //is required.
//        System.out.println("Before formComponentResized(): size:"+this.getSize()+" pref:"+this.getPreferredSize());
//        this.setPreferredSize(this.getSize());
        adjustSpaceSize();
//        resize();
//                System.out.println("After formComponentResized(): size:"+this.getSize()+" pref:"+this.getPreferredSize());
//        System.out.println("PBPanel:"+this.getSize());
    }//GEN-LAST:event_formComponentResized

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables

    private void win() {
        gameStopped=true;
        mainPanel.win();
    }

    private void lose() {
        gameStopped=true;
        mainPanel.lose();
    }
}
